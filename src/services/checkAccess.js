import { Auth } from 'aws-amplify';

async function checkAccess() {
    let userSession = null;

    await Auth.currentSession()
        .then((session) => {
            const idTokenExpire = session.getIdToken().getExpiration();
            const refreshToken = session.getRefreshToken();
            const currentTimeSeconds = Math.round(+new Date() / 1000);

            if (idTokenExpire < currentTimeSeconds) {
                Auth.currentAuthenticatedUser()
                    .then((user) => {
                        user.refreshSession(refreshToken, (error, session) => {
                            if (error) {
                                Auth.signOut({ global: true });
                                handleErrors(error);
                            } else {
                                userSession = session;
                            }
                        });
                    });
            } else {
                userSession = session;
            }
        })
        .catch((error) => {
            // No logged-in user: don't set auth header;
            handleErrors(error);
        });

    return userSession;
}

async function login(login, password) {
    let userSession = null;

    await Auth.signIn(login, password)
        .then(session => {
            if (session.challengeName === 'NEW_PASSWORD_REQUIRED') {
                Auth.completeNewPassword(session, password)
                    .then(session => {
                        userSession = session;
                    })
                    .catch(error => {
                        handleErrors(error);
                    });
            } else {
                userSession = session;
            }
        })
        .catch(error => {
            handleErrors(error);
        })

    return userSession;
}

function handleErrors(error) {
    console.log(error);
}

export {
    checkAccess,
    login,
}