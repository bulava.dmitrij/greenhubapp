import axios from 'axios';
import { checkAccess } from "../services/checkAccess";

const greenHubAPI = axios.create({
    baseURL: 'https://api.greenHub',
});

greenHubAPI.interceptors.request.use(function (config) {
    return new Promise((resolve, reject) => {
       checkAccess()
            .then(session => {
                if (session) {
                    config.headers.Authorization = "Bearer " + session.getIdToken().getJwtToken();
                    resolve(config);
                } else {
                    reject('No access');
                }
            })
           .catch(error => {
               reject(error)
           });
    })
})

export default greenHubAPI