import { createRouter, createWebHistory } from 'vue-router'
import UserLogin from '../views/UserLogin.vue'
import AdminPanel from '../views/AdminPanel.vue'
import { checkAccess } from "../services/checkAccess";

const routes = [
    {
        path: '/',
        name: 'Admin',
        component: AdminPanel,

        async beforeEnter(to, from, next) {
            await checkAccess()
                .then(userSession => {
                    if (!userSession) {
                        router.push({name: 'Login'});
                    }

                    next();
                });

        }
    },
    {
        path: '/login',
        name: 'Login',
        component: UserLogin
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
